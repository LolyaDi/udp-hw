﻿using System;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;

namespace ScreenshotApp
{
    public class Program
    {
        private const int BUFFER_SIZE = 1024 * 63;
        private static byte[] _buffer;

        private static readonly string _filePath = $"{Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)}\\screenshot.png";
        private static readonly int _localPort = 10002;
        private static readonly int _remotePort = 10001;
        private static readonly string _remoteIp = "192.168.56.1";

        private static UdpClient _server;

        private static void MakeScreenshot()
        {
            var screenshot = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            using (var graphics = Graphics.FromImage(screenshot))
            {
                graphics.CopyFromScreen(0, 0, 0, 0, Screen.PrimaryScreen.Bounds.Size);
                screenshot.Save(_filePath);
            }
        }

        private static void Send()
        {
            _buffer = new byte[BUFFER_SIZE];

            try
            {
                using (var fileStream = new FileStream(_filePath, FileMode.Open, FileAccess.Read))
                using (var binaryReader = new BinaryReader(fileStream))
                {
                    _buffer = binaryReader.ReadBytes((int)fileStream.Length);
                }

                using (_server = new UdpClient(new IPEndPoint(IPAddress.Any, _localPort)))
                {
                    _server.Connect(new IPEndPoint(IPAddress.Parse(_remoteIp), _remotePort));
                    _server.Send(_buffer, BUFFER_SIZE);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        private static void Main(string[] args)
        {
            ThreadPool.QueueUserWorkItem((state) =>
            {
                MakeScreenshot();
                Send();
            });

            Console.ReadLine();
        }
    }
}
